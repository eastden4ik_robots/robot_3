package ru.sviridoff.robots.robot_3;

class Xpath {

    final static String GoToLogin = "//a[contains(text(),'Вход и регистрация')]";
    final static String LoginWithPwd = "//label[contains(text(),'Вход с паролем')]";
    final static String LoginInput = "//input[@name='sign']";
    final static String PasswordInpt = "//input[@name='password']";
    final static String LogInBtn = "//button[@id='signbutton']";
    final static String LinkWithUsername = "//td[@class='col_login']/a";
    final static String LinkToLK = "//a[contains(text(),'Личный кабинет')]";
    final static String LogoLink = "//td[contains(@class,'col_logo')]";
    final static String LKPage = ".//h1[contains(@class, 'user-profile-title')]/span[contains(text(), 'Den0303')]";
    final static String AmountRub = ".//div[@class='payCol']/div[@class='personal-balance-info__balance']";
    final static String AmountOfDays = ".//div[@class='payCol']/div[contains(@class,'predictionDaysHolder ')]//b";

}
