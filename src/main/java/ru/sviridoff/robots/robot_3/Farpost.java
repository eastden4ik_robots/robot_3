package ru.sviridoff.robots.robot_3;

import io.qameta.allure.model.Status;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.openqa.selenium.By;
import ru.sviridoff.framework.driver.SeleniumUtils;

import javax.mail.MessagingException;

import java.util.Arrays;

import static io.qameta.allure.Allure.step;
import static ru.sviridoff.robots.robot_3.Stash.*;

class Farpost {


    private final SeleniumUtils seleniumUtils = new SeleniumUtils();

    void logIn() {

        driver.get((String) parameters.getValue("farpost.url"));
        seleniumUtils.isPresence(driver, By.xpath(Xpath.LogoLink), 20);

        driver.findElement(By.xpath(Xpath.GoToLogin)).click();
        seleniumUtils.isPresence(driver, By.xpath(Xpath.LoginWithPwd), 20);

        driver.findElement(By.xpath(Xpath.LoginWithPwd)).click();
        seleniumUtils.isPresence(driver, By.xpath(Xpath.LoginInput), 20);

        driver.findElement(By.xpath(Xpath.LoginInput)).clear();
        driver.findElement(By.xpath(Xpath.LoginInput)).sendKeys((String) parameters.getValue("farpost.login"));
        driver.findElement(By.xpath(Xpath.PasswordInpt)).clear();
        driver.findElement(By.xpath(Xpath.PasswordInpt)).sendKeys((String) parameters.getValue("farpost.pwd"));

        driver.findElement(By.xpath(Xpath.LogInBtn)).click();
        seleniumUtils.isPresence(driver, By.xpath(Xpath.LinkWithUsername), 20);
        step("Авторизация успешна", Status.PASSED);
    }

    void checkAmountRub() {

        driver.findElement(By.xpath(Xpath.LinkWithUsername)).click();
        seleniumUtils.isPresence(driver, By.xpath(Xpath.LinkToLK), 20);

        driver.findElement(By.xpath(Xpath.LinkToLK)).click();
        seleniumUtils.isPresence(driver, By.xpath(Xpath.LKPage), 20);
        seleniumUtils.isPresence(driver, By.xpath(Xpath.AmountRub), 20);
        seleniumUtils.isPresence(driver, By.xpath(Xpath.AmountOfDays), 20);

        AmountRub = driver.findElement(By.xpath(Xpath.AmountRub)).getText().split("р\\.")[0].trim();
        AmountDays = driver.findElement(By.xpath(Xpath.AmountOfDays)).getText().replaceAll("→", "");
        step("Робот проверил баланс и остаток дней по объявлениям. " + AmountRub + " руб., " + AmountDays, Status.PASSED);
    }

    void whatsAppNotification(boolean isWhatsApp, String message) {
        if (isWhatsApp) {
            String contact_1 = (String) parameters.getValue("whatsapp.contact.1");
            String contact_2 = (String) parameters.getValue("whatsapp.contact.2");
            Unirest.get("http://localhost:7778/whatsapp/send/{message}/to/{to}")
                    .routeParam("message", message)
                    .routeParam("to", contact_1)
                    .asJsonAsync(httpResponse -> {
                        int code = httpResponse.getStatus();
                    });
            Unirest.get("http://localhost:7778/whatsapp/send/{message}/to/{to}")
                    .routeParam("message", message)
                    .routeParam("to", contact_2)
                    .asJsonAsync(httpResponse -> {
                        int code = httpResponse.getStatus();
                    });
            String[] contacts = ((String) parameters.getValue("whatsapp.contacts")).split(";");
//            Arrays.asList(contacts).forEach(contact -> {
//                HttpResponse<JsonNode> response = Unirest.get("http://localhost:7778/whatsapp/send/{message}/to/{to}")
//                        .routeParam("message", message)
//                        .routeParam("to", contact)
//                        .asJson();
//                System.out.println(response.getStatus() + ":" + response.getStatusText() + "; " + response.getBody().toPrettyString());
//            });


        } else
            step("Робот не отправлял пользователям результат в WhatsApp. Пропуск.", Status.SKIPPED);
    }

    void tgNotification(boolean isTG, String message) {
        if (isTG) {
            String userId = (String) parameters.getValue("tg.user.id_1");
            boolean telegramNotificator = telegram.send(userId, message);
            if (telegramNotificator)
                step("Сообщение в телеграм пользователю " + userId + " отправлено.", Status.PASSED);
            else
                step("Сообщение в телеграм пользователю " + userId + " не отправлено.", Status.FAILED);
        } else
            step("Робот не отправлял пользователям результат в телегу. Пропуск.", Status.SKIPPED);
    }

    void mailNotification(boolean isMail) throws MessagingException {
        if (isMail) {
            String content = "Робот проверил баланс и остаток дней по объявлениям. " + AmountRub + " руб., " + AmountDays;
            mail.sendMessage("Робот проверки баланса на фарпосте.", content, (String) parameters.getValue("address.to"));
            step("Робот отправил пользователям результат: " + parameters.getValue("address.to"), Status.PASSED);
        } else
            step("Робот не отправлял пользователям результат на почту. Пропуск.", Status.SKIPPED);
    }

}
