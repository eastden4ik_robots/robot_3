package ru.sviridoff.robots.robot_3.bots;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import ru.sviridoff.framework.logger.Logger;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


public class Telegram {

    private String TOKEN;
    private String baseUrl = "https://api.telegram.org/bot";

    private Logger log = new Logger(Telegram.class.getSimpleName());

    public Telegram(String token) {
        this.TOKEN = token;
    }


    public boolean send(String chatId, String text) {
        HttpResponse<?> request = Unirest.get(this.baseUrl + this.TOKEN + "/sendMessage")
                .queryString("chat_id", chatId)
                .queryString("text", text)
                .asJson();
        if (request.isSuccess()) {
            log.info("Response code: {" + request.getStatus() + "}, response text: {" + request.getStatusText() + "}.");
        } else {
            log.error("Response code: {" + request.getStatus() + "}, response text: {" + request.getStatusText() + "}.");
        }
        return request.isSuccess();
    }

}
