package ru.sviridoff.robots.robot_3;

import ru.sviridoff.framework.MailClasses.Mail;
import ru.sviridoff.framework.allure.AllureReporter;
import ru.sviridoff.framework.driver.SeleniumDriver;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Parameters;
import ru.sviridoff.robots.robot_3.bots.Telegram;

import static ru.sviridoff.robots.robot_3.Stash.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import static io.qameta.allure.Allure.step;

public class Robot_3 {

    private static final Farpost farpost = new Farpost();
    private static final SeleniumDriver seleniumDriver = new SeleniumDriver();

    private static final SimpleDateFormat df = new SimpleDateFormat("[dd MMMM yyyy: HH:mm:ss]");

    private static Logger logger;
    private static boolean isMail;
    private static boolean isTG;
    private static boolean isWhatsApp;

    public static void setUp() {

        Parameters params = new Parameters(System.getenv("env"));
        OS os = getOS((String) params.getValue("system"));
        parameters = params;
        logger = new Logger(Robot_3.class.getSimpleName(), (String) parameters.getValue("log.file"));

        isMail = parameters.getValue("mail.flag").equals("true");
        isTG = parameters.getValue("tg.flag").equals("true");
        isWhatsApp = parameters.getValue("whatsapp.flag").equals("true");

        switch (os) {
            case WIN: {
                driver = seleniumDriver.Init(SeleniumDriver.Drivers.YANDEX_WINDOWS);
                break;
            }
            case LINUX: {
                driver = seleniumDriver.Init(SeleniumDriver.Drivers.CHROME_LINUX);
                break;
            }
            case HEADLESS_YANDEX: {
                driver = seleniumDriver.Init(SeleniumDriver.Drivers.YANDEX_HEADLESS_WIN);
                break;
            }
            default: {
                driver = seleniumDriver.Init(SeleniumDriver.Drivers.CHROME_HEADLESS_WIN);
            }
        }
        if (isMail) {
            mail = new Mail(
                    (String) parameters.getValue("host"),
                    (String) parameters.getValue("port"),
                    (String) parameters.getValue("login"),
                    (String) parameters.getValue("pwd"),
                    (String) parameters.getValue("address.from")
            );
        }
        if (isTG) {
            telegram = new Telegram((String) parameters.getValue("tg.token"));
        }
        logger.info("Переменные настроены и робот готов к работе.");
    }

    private static void destroy() {
        seleniumDriver.Destroy(driver);
        logger.info("Робот закончил свою работу. Спасибо за то, что были с нами.");
    }

    public static void main(String[] args) {

        setUp();
        logger.info("Application started.");
        AllureReporter.setUp("RPA by Sviridoff. Robot 3. Проверка баланса на фарпосте.", Robot_3.class.getSimpleName());
        try {
            step("Авторизация робота.", () -> {
                farpost.logIn();
                logger.info("Робот успешно авторизовался на фарпосте.");
            });
            AtomicReference<String> message = new AtomicReference<>();
            step("Проверка баланса на фарпосте.", () -> {
                logger.info("Проверка баланса на фарпосте.");
                farpost.checkAmountRub();
                logger.info("Баланс успешно получен.");
                message.set(df.format(new Date()) +
                        "\n[" + Robot_3.class.getSimpleName() + "-Farpost" + "]" +
                        "\nРобот проверил баланс и остаток дней по объявлениям на сайте farpost." +
                        "\nОстаток денег - " + AmountRub + "," +
                        "\nОстаток дней - " + AmountDays);
                logger.info(message.get());
            });
            step("Отправка уведомлений", () -> {
                farpost.mailNotification(isMail);
                farpost.tgNotification(isTG, message.get());
                farpost.whatsAppNotification(isWhatsApp, message.get());
                logger.info("Робот отправил уведомления о работе.");
            });

            destroy();
        } catch (Exception exception) {
            AllureReporter.processException(exception);
        } finally {
            AllureReporter.tearDown();
        }
    }

    enum OS {
        WIN, LINUX, HEADLESS_YANDEX, UNDEFINED
    }

    private static OS getOS(String os) {
        if (os.equalsIgnoreCase("win"))
            return OS.WIN;
        else if (os.equalsIgnoreCase("linux"))
            return OS.LINUX;
        else if (os.equalsIgnoreCase("headless yandex"))
            return OS.HEADLESS_YANDEX;
        else return OS.UNDEFINED;
    }


}
